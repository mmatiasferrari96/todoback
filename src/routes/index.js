const { Router } = require("express");
const { db } = require("../firebase");

const router = Router();

router.get("/task", async (req, res) => {
  const querySnapshot = await db.collection("toDo").get();
  const task = querySnapshot.docs.map(doc => ({ 
    id: doc.id,
    ...doc.data(),
  }))

  res.send(task);
});

router.post('/newTask', async (req, res) => {
    
    const { date, description, userId } = req.body
    await db.collection("toDo").add({ 
        date,
        description,
        userId
    })
    res.send("new task!");
})

module.exports = router;

Paso 1: 

*Configurar las variables de entorno

-Crea un archivo .env en la raíz del proyecto si no existe.

-Abre el archivo .env y agrega la siguiente línea:

    GOOGLE_APPLICATION_CREDENTIALS=/ruta/absoluta/a/firebase.json

Reemplaza /ruta/absoluta/a/firebase.json con la ruta absoluta al archivo firebase.json

Paso 2:

*Instalar dependencias y ejecutar la aplicación

-Abre una terminal en la ubicación del proyecto.

-Ejecuta "npm install"

-Una vez completada la instalación, ejecuta "npm run dev" para iniciar la aplicación:


*La aplicación se ejecutará en modo de desarrollo en "http://localhost:3000".